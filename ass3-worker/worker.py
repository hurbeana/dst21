import signal
from contextlib import contextmanager
from json import loads, dumps
from random import randint
from sys import exit
from time import perf_counter, sleep

import click
import pika
from geopy.distance import geodesic
from pika import spec
from pika.adapters.blocking_connection import BlockingChannel
from redis import Redis

region_wait_times = {"at_linz": (1, 2), "at_vienna": (3, 5), "de_berlin": (8, 11)}


def handle_sigterm(signum, frame):
    print("SIGTERM received, worker shutting down ...")
    exit(0)


@contextmanager
def catchtime() -> float:
    start = perf_counter()
    yield lambda: perf_counter() - start


@click.command()
@click.argument("region", type=str)
@click.option("--rmq_host", default="192.168.99.99", help="RabbitMQ host to connect to")
@click.option("--rmq_port", default="5672", help="RabbitMQ port to connect to")
@click.option("--rmq_vhost", default="/", help="RabbitMQ vhost to connect to")
@click.option("-u", "--rmq_user", default="dst", help="RabbitMQ user")
@click.option("-p", "--rmq_password", default="dst", help="RabbitMQ users password")
@click.option("--redis_host", default="192.168.99.99", help="Redis host to connect to")
def main(region, rmq_host, rmq_port, rmq_vhost, rmq_user, rmq_password, redis_host):
    """ Start a worker that runs the matching algorithm for the givem REGION """

    def process_request(
        ch: BlockingChannel,
        method: spec.Basic.Deliver,
        properties: spec.BasicProperties,
        body: bytes,
    ):
        """
        Processes one request in form of TripRequest and writes the response to
        the requests topic queues form the elasticity controller
        """
        with catchtime() as t:
            success = False
            request = loads(body)
            print(f"Processing TripRequest with id {request['id']}")

            r = Redis(host=redis_host)
            key = f"drivers:{region}"
            drivers = r.hgetall(key)
            closest = ""
            closest_dist = float("inf")
            if drivers:
                sleep(randint(*region_wait_times.get(region)))  # the random work
                pickup_loc = (request["latitude"], request["longitude"])
                for key, val in drivers.items():
                    driver_loc = (float(x) for x in val.split(" "))
                    dist = geodesic(pickup_loc, driver_loc)
                    if dist < closest_dist:
                        closest_dist = dist
                        closest = key

                print(f" [!] Found closests driver [id={closest}, dist={closest_dist}")
                resp = r.hdel(key, closest)
                if not resp:
                    # driver not available anymore, just retry
                    process_request(ch, method, properties, body)
                success = True
            else:
                print(f" [!] No drivers available currently")

        response = {
            "requestId": request["id"],
            "processingTime": t() if success else 0,
            "driverId": closest,
        }
        ch.basic_publish(
            exchange="dst.workers",
            routing_key=f"requests.{region}",
            body=dumps(response).encode("utf-8"),
        )

    signal.signal(signal.SIGTERM, handle_sigterm)
    queue_name = f"dst.{region}"
    print(
        f" [*] Connecting to RabbitMQ with [{rmq_user}:{rmq_password}@{rmq_host}:{rmq_port}]"
    )
    conn = pika.BlockingConnection(
        pika.ConnectionParameters(
            host=rmq_host,
            port=rmq_port,
            virtual_host=rmq_vhost,
            credentials=pika.PlainCredentials(rmq_user, rmq_password),
        )
    )
    channel = conn.channel()
    channel.queue_declare(queue=queue_name, durable=True)

    channel.basic_consume(
        queue=queue_name, auto_ack=False, on_message_callback=process_request
    )

    print(" [*] Listening for requests. Press CTRL+C to stop ...")
    channel.start_consuming()

if __name__ == '__main__':
    main()