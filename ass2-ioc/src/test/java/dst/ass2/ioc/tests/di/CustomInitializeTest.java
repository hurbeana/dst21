package dst.ass2.ioc.tests.di;

import dst.ass2.ioc.di.IObjectContainer;
import dst.ass2.ioc.di.IObjectContainerFactory;
import dst.ass2.ioc.di.annotation.Component;
import dst.ass2.ioc.di.annotation.Initialize;
import dst.ass2.ioc.di.impl.ObjectContainerFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;

public class CustomInitializeTest {

    private IObjectContainerFactory factory;
    private IObjectContainer container;

    @Before
    public void setUp() {
        factory = new ObjectContainerFactory();
        container = factory.newObjectContainer(new Properties());

        if (container == null) {
            throw new NullPointerException("ObjectContainerFactory did not return an ObjectContainer instance");
        }
    }

    @Component
    public static class ComponentA {

        protected AtomicInteger initializeCalls = new AtomicInteger(0);

        @Initialize
        public void myInitMethod() {
            initializeCalls.incrementAndGet();
        }

        public int getInitializeCalls() {
            return initializeCalls.get();
        }
    }

    @Component
    public static class ComponentB extends ComponentA {
        @Initialize
        public void mySecondInitMethod() { initializeCalls.incrementAndGet(); }
    }

    @Test
    public void getObject_runsInitializeMethodInHierarchy() throws Exception {
        CustomInitializeTest.ComponentB component = container.getObject(CustomInitializeTest.ComponentB.class);
        assertEquals("expected two calls to @Intialize Methods", 2, component.getInitializeCalls());
    }

    @Component
    public static class ComponentC extends ComponentA {
        @Initialize
        @Override
        public void myInitMethod() { initializeCalls.incrementAndGet(); }
    }

    @Test
    public void getObject_runsInitializeMethodOnceInHierarchyWithOverride() throws Exception {
        CustomInitializeTest.ComponentC component = container.getObject(CustomInitializeTest.ComponentC.class);
        assertEquals("expected one call to @Intialize Methods", 1, component.getInitializeCalls());
    }

    @Component
    public static class ComponentD extends ComponentA {
        @Initialize
        public void myInitMethod() { initializeCalls.incrementAndGet(); }
    }

    @Test
    public void getObject_runsInitializeMethodOnceInHierarchyWithoutOverride() throws Exception {
        CustomInitializeTest.ComponentD component = container.getObject(CustomInitializeTest.ComponentD.class);
        assertEquals("expected one call to @Intialize Methods", 1, component.getInitializeCalls());
    }
}
