package dst.ass2.ioc.tests;

import dst.ass2.ioc.tests.di.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DependencyInjectionTest.class,
        PropertyInjectionTest.class,
        HierarchyTest.class,
        InitializeTest.class,
        CustomInitializeTest.class
})
public class Ass2_2_1_Suite {
    // suite
}
