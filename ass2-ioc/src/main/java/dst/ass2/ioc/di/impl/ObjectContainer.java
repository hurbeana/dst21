package dst.ass2.ioc.di.impl;

import dst.ass2.ioc.di.*;
import dst.ass2.ioc.di.annotation.*;

import java.lang.reflect.*;
import java.util.*;

@SuppressWarnings("ALL")
public class ObjectContainer implements IObjectContainer {

    private final Properties properties;
    private final HashMap<Type, Object> singletonMap;
    private final HashMap<Class<?>, Class<?>> boxer;

    public ObjectContainer(Properties properties) {
        singletonMap = new HashMap<>();
        boxer = new HashMap<>();
        boxer.put(boolean.class, Boolean.class);
        boxer.put(byte.class, Byte.class);
        boxer.put(char.class, Character.class);
        boxer.put(float.class, Float.class);
        boxer.put(int.class, Integer.class);
        boxer.put(long.class, Long.class);
        boxer.put(short.class, Short.class);
        boxer.put(double.class, Double.class);

        this.properties = properties;
    }

    /**
     * Returns the (mutable) data structure holding property values that can be injected via the
     * {@link dst.ass2.ioc.di.annotation.Property} annotation.
     *
     * @return A mutable Properties object
     */
    @Override
    public Properties getProperties() {
        return properties;
    }

    /**
     * Returns a container-managed object of the given type.
     *
     * @param type the type of object
     * @return the object
     * @throws InjectionException throw the concrete InjectionException as specified in the assignment
     */
    @Override
    public <T> T getObject(Class<T> type) throws InjectionException {
        Component componentAnnotation = type.getDeclaredAnnotation(Component.class);
        if (componentAnnotation == null) // Continue only if class has @Component
            throw new InvalidDeclarationException("Could not inject annotation");
        if(componentAnnotation.scope() == Scope.SINGLETON) {
            synchronized(singletonMap) { // critical section, only one at a time, to not mess with object creation
                if (singletonMap.containsKey(type)) {
                    return (T) singletonMap.get(type);  // already constructed, return it
                }
                T component = createObject(type);
                singletonMap.put(type, component);
                return component;
            }
        }
        return createObject(type);
    }

    private <T> T createObject(Class<T> type) throws InjectionException {
        Properties fixedProperties = (Properties)properties.clone();
        // get the constructor with 0 args
        Constructor<?> zeroConstructor = Arrays.stream(type.getDeclaredConstructors())
                .filter(constructor -> constructor.getParameterCount() == 0)
                .findFirst()
                .orElseThrow(() -> new InjectionException("Could not find ctor with zero args"));

        zeroConstructor.setAccessible(true); // so that we can have access to a private ctor
        T component;
        try {
            component = (T) zeroConstructor.newInstance(); // Construct object
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new ObjectCreationException(e);
        }
        // Inject all fields with @Inject and @Property
        injectFields(type, component, fixedProperties);
        // call initialize
        callInitializes(type, component);
        return component;
    }

    private <T> void injectFields(Class<T> type, T component, Properties fixedProperties) {
        if (type.getSuperclass() != null) injectFields(type.getSuperclass(), component, fixedProperties);
        for (Field field : type.getDeclaredFields()) {
            field.setAccessible(true); // if its private, so we can access it
            Class<?> fieldType = field.getType();
            Inject injectAnnotation = field.getAnnotation(Inject.class);
            Property propertyAnnotation = field.getAnnotation(Property.class);
            if (injectAnnotation != null) { // do the injection
                Class<?> injectType = injectAnnotation.targetType() == Void.class
                        ? field.getType() : injectAnnotation.targetType();
                try {
                    field.set(component, getObject(injectType)); // get managed object
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    if (!injectAnnotation.optional()) throw new InvalidDeclarationException(e);
                }
            } else if (propertyAnnotation != null) { // do the property setting
                try {
                    String propertyName = propertyAnnotation.value();
                    if(propertyName.equals("")) throw new ObjectCreationException("Property value (in annotation) not set");
                    Object propertyValue = fixedProperties.get(propertyName);
                    if(propertyValue == null) throw new ObjectCreationException("Property value (in Properties) not set");
                    if(field.getType() != String.class) {
                        Class<?> toSearch = boxer.getOrDefault(fieldType, fieldType);
                        Method parseMethod = Arrays.stream(toSearch.getMethods())
                                .filter(x -> x.getName().startsWith("parse") && x.getParameterCount() == 1)
                                .findFirst()
                                .orElseThrow(() -> new TypeConversionException("Couldn't find suitable method to convert"));
                        propertyValue = parseMethod.invoke(propertyValue, propertyValue);
                    }
                    field.set(component, propertyValue);
                } catch (IllegalAccessException e) {
                    throw new ObjectCreationException(e);
                } catch(ClassCastException | InvocationTargetException e) {
                    throw new TypeConversionException(e);
                }
            }
        }
    }

    private <T> void callInitializes(Class<T> type, T component) {
        callInitializesRec(type, component, new LinkedList<>());
    }

    private <T> void callInitializesRec(Class<T> type, T component, List<String> called) {
        try {
            for (Method method : type.getDeclaredMethods()) {
                Initialize initializeAnnotation = method.getAnnotation(Initialize.class);
                    if (initializeAnnotation == null || called.contains(method.getName()))
                        continue;
                if (method.getParameterCount() != 0)
                    throw new InvalidDeclarationException("Initialize methods are not allowed to have parameters");
                method.setAccessible(true);
                method.invoke(component);
                called.add(method.getName());
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new ObjectCreationException(e);
        }
        if (type.getSuperclass() != null) callInitializesRec(type.getSuperclass(), component, called);
    }
}
