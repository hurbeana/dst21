package dst.ass2.ioc.lock;

import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

public class LockManager {

    private static final LockManager INSTANCE = new LockManager(); // Runtime initialization
    private final HashMap<String, ReentrantLock> locks = new HashMap<>();

    // see here: https://stackoverflow.com/a/2912343
    // and here: https://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom
    public static LockManager getInstance() {
        return INSTANCE; // should be threadsafe due to INSTANCE being created once during class-load time
    }

    public ReentrantLock getLock(String name) {
        synchronized (locks) {
            if (locks.containsKey(name))
                return locks.get(name);
            ReentrantLock newLock = new ReentrantLock();
            locks.put(name, newLock);
            return newLock;
        }
    }
}
