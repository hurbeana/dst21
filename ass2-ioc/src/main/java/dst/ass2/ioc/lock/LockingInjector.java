package dst.ass2.ioc.lock;

import dst.ass2.ioc.di.annotation.Component;
import javassist.*;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public class LockingInjector implements ClassFileTransformer {

    @Override
    public byte[] transform(ClassLoader loader, String className,
                            Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) throws IllegalClassFormatException {

        // TODO transform all @Lock annotated methods of classes with a @Component annotation
        ClassPool classPool = ClassPool.getDefault();
        CtClass ctClass;
        String lockManagerClass = "dst.ass2.ioc.lock.LockManager";
        try {
            ctClass = classPool.get(className.replace("/", "."));
            if(ctClass.getAnnotation(Component.class) == null){
                return classfileBuffer;
            }
        } catch (NotFoundException | ClassNotFoundException e) {
            throw new RuntimeException("Failed processing class " + className + " due to " + e.getMessage());
        }
        for(CtMethod cMethod : ctClass.getMethods()) {
            Lock lockAnnotation;
            try {
                lockAnnotation = (Lock) cMethod.getAnnotation(Lock.class);
                if(lockAnnotation == null) continue;
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("Failed processing Lock, Lock interface doesn't exist (" + e.getMessage() + ")");
            }
            String lockAnnotationValue = lockAnnotation.value();

            String insertBefore = String.format("%s.getInstance().getLock(\"%s\").lock();", lockManagerClass, lockAnnotationValue);

            String insertAfter = String.format("%s.getInstance().getLock(\"%s\").unlock();", lockManagerClass, lockAnnotationValue);

            try {
                System.out.println(" ------------------------>>>>>>>> Adding lockingmanager to " + ctClass.getName());
                cMethod.insertBefore(insertBefore);
                // asFinally: true, so that this always gets executed to release the lock
                cMethod.insertAfter(insertAfter, true);
            } catch (CannotCompileException e) {
                e.printStackTrace();
                throw new RuntimeException("Failed to compile instrumentation for locking");
            }
        }

        try {
            ctClass.detach();
            return ctClass.toBytecode();
        } catch (IOException | CannotCompileException e) {
            return classfileBuffer;
        }
    }

}
