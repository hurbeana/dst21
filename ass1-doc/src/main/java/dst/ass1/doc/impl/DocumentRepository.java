package dst.ass1.doc.impl;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import dst.ass1.doc.IDocumentRepository;
import dst.ass1.jpa.model.ILocation;
import dst.ass1.jpa.util.Constants;
import org.bson.Document;

import java.util.Map;

public class DocumentRepository implements IDocumentRepository {

    protected final MongoCollection<Document> col;

    public DocumentRepository() {
        MongoClient mongoClient = MongoClients.create();
        MongoDatabase db = mongoClient.getDatabase(Constants.MONGO_DB_NAME);
        col = db.getCollection(Constants.COLL_LOCATION_DATA);

        col.createIndex(Indexes.geo2dsphere("geo.coordinates"));
        IndexOptions indexOptions = new IndexOptions().unique(true);
        col.createIndex(Indexes.ascending(Constants.I_LOCATION), indexOptions);
    }
    @Override
    public void insert(ILocation location, Map<String, Object> locationProperties) {
        Document doc = new Document(locationProperties)
                .append(Constants.I_LOCATION, location.getLocationId())
                .append(Constants.M_LOCATION_NAME, location.getName());
        col.insertOne(doc);
    }
}
