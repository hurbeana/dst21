package dst.ass1.doc.impl;

import static com.mongodb.client.model.Filters.*;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import dst.ass1.doc.IDocumentQuery;
import dst.ass1.jpa.util.Constants;
import org.bson.Document;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class DocumentQuery implements IDocumentQuery {

    protected final MongoCollection<Document> col;

    public DocumentQuery(MongoDatabase db) {
        col = db.getCollection(Constants.COLL_LOCATION_DATA);
    }

    @Override
    public Document findLocationById(Long locationId) {
        Document loc = col.find(eq(Constants.I_LOCATION, locationId)).first();
        if(loc != null) {
            System.out.println("found location by ID [" + loc.toJson() + "]");
            return loc;
        }
        System.out.println("Couldn't find location by ID [" + locationId + "]");
        return null;
    }

    @Override
    public List<Long> findIdsByNameAndRadius(String name, double longitude, double latitude, double radius) {
        List<Long> res = col
            .find(and(
                    regex(Constants.M_LOCATION_NAME, ".*" + name + ".*", "i"),
                    near("geo.coordinates", new Point(new Position(longitude, latitude)), radius, (double) 0)
            ))
                .projection(Projections.include(Constants.I_LOCATION))
                .into(new LinkedList<>())
                .stream()
                .map(x -> x.getLong(Constants.I_LOCATION))
                .collect(Collectors.toList());
        System.out.println("Found following session ids " + res.toString());
        return res;
    }

    @Override
    public List<Document> getDocumentStatistics() {
        String jsMapFunction =
                "function() {" +
                        "if(this.type == 'place') emit(this.category, 1);" +
                        "}";
        String jsReduceFunction =
                "function(key, values) {" +
                        "return Array.sum(values)" +
                        "}";
        List<Document> res = col.mapReduce(jsMapFunction, jsReduceFunction).into(new LinkedList<>());
        System.out.println("Found following places: " + res);
        return res;
    }
}
