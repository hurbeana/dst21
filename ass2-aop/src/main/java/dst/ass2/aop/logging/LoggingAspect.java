package dst.ass2.aop.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.logging.*;

import static org.springframework.util.ReflectionUtils.findField;

@Aspect
public class LoggingAspect {

    @Pointcut("!@annotation(dst.ass2.aop.logging.Invisible) && " +
            "execution(void dst.ass2.aop.IPluginExecutable.execute())")
    private void logPointCut() {}

    @Around("logPointCut()")
    public void logAdvice(ProceedingJoinPoint thisJoinPoint) throws Throwable {
        Class<?> targetClass = thisJoinPoint.getTarget().getClass();
        Logger logger = getLogger(targetClass, thisJoinPoint.getThis());
        if(logger == null) {
            SimpleFormatter formatter = new SimpleFormatter();
            System.out.println(formatter.format(new LogRecord(Level.INFO, targetClass.getName() + " started execute")));
            thisJoinPoint.proceed();
            System.out.println(formatter.format(new LogRecord(Level.INFO, targetClass.getName() + " finished execute")));
        } else {
            logger.setLevel(Level.INFO);
            logger.info(targetClass.getName() + " started execute");
            thisJoinPoint.proceed();
            logger.info(targetClass.getName() + " finished execute");
        }
    }

    private Logger getLogger(Class<?> clazz, Object o) {
        for(Field field : clazz.getDeclaredFields()) {
            if(field.getType() == Logger.class) {
                field.setAccessible(true);
                try {
                    return (Logger)field.get(o);
                } catch (IllegalAccessException ignored) { }
            }
        }
        return null;
    }
}
