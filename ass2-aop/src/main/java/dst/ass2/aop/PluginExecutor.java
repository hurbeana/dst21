package dst.ass2.aop;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Stream;

public class PluginExecutor implements IPluginExecutor {

    private final WatchService watcher;
    private final HashMap<String, WatchKey> keys;
    private Thread watcherThread;
    private final ExecutorService executor;

    public PluginExecutor() {
        try {
            this.watcher = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            throw new RuntimeException("Could not get default WatchService");
        }
        keys = new HashMap<>();
        executor = Executors.newCachedThreadPool();
    }

    /**
     * Adds a directory to monitor.
     * May be called before and also after start has been called.
     *
     * @param dir the directory to monitor.
     */
    @Override
    public void monitor(File dir) {
        Path dirPath = Paths.get(dir.toURI());
        if(keys.containsKey(dir.getAbsolutePath())) return;
        try {
            WatchKey key = dirPath.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);
            keys.put(dir.getAbsolutePath(), key);
        } catch (IOException e) {
            throw new RuntimeException("Could not register directory " + dir.getAbsolutePath() + " for watching");
        }
    }

    /**
     * Stops monitoring the specified directory.
     * May be called before and also after start has been called.
     *
     * @param dir the directory which should not be monitored anymore.
     */
    @Override
    public void stopMonitoring(File dir) {
        String absolutePath = dir.getAbsolutePath();
        if(keys.containsKey(absolutePath)){
            keys.get(absolutePath).cancel(); // set key to cancel state
        }
    }

    /**
     * Starts the plugin executor.
     * All added directories will be monitored and any .jar file processed.
     * If there are any {@link IPluginExecutable} implementations,
     * they are executed within own threads.
     */
    @Override
    public void start() {
        if(watcherThread == null) {
            watcherThread = new Thread(this::processEvents);
            watcherThread.start();
        }

        for(Map.Entry<String, WatchKey> e : keys.entrySet()){
            Path dir = (Path)e.getValue().watchable();
            File file = dir.resolve(dir).toFile();

            if(file.isDirectory()){
                // try-with-resource
                // https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
                try (Stream<Path> paths = Files.walk(Paths.get(file.toURI()))){
                    paths.forEach(this::scanJAR);
                }
                catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Stops the plugin executor.
     * The monitoring of directories and the execution
     * of the plugins should stop as soon as possible.
     */
    @Override
    public void stop() {
        watcherThread.interrupt();
    }

    private void processEvents()  {
        for(;;) {
            WatchKey key;
            List<Path> alreadyRan = new LinkedList<>();
            try {
                key = watcher.take();
            } catch (InterruptedException e) {
                return;
            }

            for (WatchEvent<?> event: key.pollEvents()) {
                WatchEvent.Kind<?> kind = event.kind();

                WatchEvent<Path> ev = (WatchEvent<Path>)event;
                Path filename = ev.context();
                if(alreadyRan.contains(filename)) continue;
                alreadyRan.add(filename);
                Path dir = (Path)key.watchable();
                Path absolutePath = dir.resolve(filename);
                scanJAR(absolutePath);
            }
            boolean valid = key.reset(); // reset the key back to ready state
            if (!valid) {
                break;
            }
        }
    }

    private void scanJAR(Path jarPath) {
        if(!jarPath.toString().endsWith(".jar")) return;

        try {
            JarFile jarFile = new JarFile(jarPath.toFile());
            Enumeration<JarEntry> entries = jarFile.entries();
            while (entries.hasMoreElements()) {
                try {
                    Class<?> loadedClass = loadClassFromJar(entries.nextElement(), jarPath);
                    IPluginExecutable object = (IPluginExecutable)loadedClass.getConstructor().newInstance();
                    executor.execute(object::execute);
                } catch (IOException | InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException ignored) { }

            }
        } catch (IOException e) {
            System.out.println("Couldn't load jar file " + jarPath.toString());
        }
    }

    private Class<?> loadClassFromJar(JarEntry jarEntry, Path jarPath) throws IOException {
        URLClassLoader classLoader;
        try {
            classLoader = URLClassLoader.newInstance(new URL[]{new URL("jar:file:" + jarPath + "!/")});
        } catch (MalformedURLException e) {
            throw new RuntimeException("Could not create a new instance of the UrlClassLoader", e);
        }
        String jarEntryName = jarEntry.getName();
        if(!jarEntryName.endsWith(".class")) throw new IOException("JarEntry not a class file");
        // replace / with . and remove the .class at the end
        String className = jarEntryName.replaceAll("/", "\\.").substring(0, jarEntryName.length() - 6);
        Class<?> loadedClass;
        try {
            loadedClass = classLoader.loadClass(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not find the class from the jar file", e);
        }

        if (Arrays.asList(loadedClass.getInterfaces()).contains(IPluginExecutable.class)) {
            return loadedClass;
        } else {
            throw new IOException("Could not load class" + className + ", because it doesn't implement the right interface");
        }
    }

}
