package dst.ass2.aop.management;

import dst.ass2.aop.IPluginExecutable;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;

@Aspect
public class ManagementAspect {

    Timer timer = new Timer();

    @Pointcut("@annotation(dst.ass2.aop.management.Timeout) && " +
            "execution(void dst.ass2.aop.IPluginExecutable.execute())")
    private void terminatePointcut(){}

    @Before("terminatePointcut()")
    public void terminateAdviceBefore(JoinPoint thisJoinPoint) {
        final MethodSignature signature = (MethodSignature) thisJoinPoint.getSignature();
        final Method method = signature.getMethod();
        final IPluginExecutable target = (IPluginExecutable) thisJoinPoint.getTarget();

        Timeout timeoutAnnotation = method.getAnnotation(Timeout.class);
        if (timeoutAnnotation == null) return;
        if(timeoutAnnotation.value() == 0) return;
        TimerTask task = new TimerTask() {
            public void run() {
                if(target != null){
                    target.interrupted();
                }
            }
        };
        timer.schedule(task, timeoutAnnotation.value());
    }

    @After("terminatePointcut()")
    public void terminateAdviceAfter(JoinPoint thisJoinPoint) {
        timer.cancel();
        timer.purge();
    }
}
