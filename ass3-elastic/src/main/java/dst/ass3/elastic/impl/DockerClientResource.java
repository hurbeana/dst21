package dst.ass3.elastic.impl;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.exception.NotFoundException;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import dst.ass3.elastic.ContainerException;
import dst.ass3.elastic.ContainerInfo;
import dst.ass3.elastic.ContainerNotFoundException;
import dst.ass3.messaging.Region;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class DockerClientResource implements AutoCloseable {

    private final DockerClient dockerClient;

    public DockerClientResource(String serverUrl) {
        DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder().withDockerHost(serverUrl).build();
        this.dockerClient = DockerClientBuilder.getInstance(config).build();
    }

    public List<ContainerInfo> listContainers() {
        List<ContainerInfo> containerInfoList = new LinkedList<>();
        for (Container container :
                this.dockerClient.listContainersCmd().exec()) {
            ContainerInfo containerInfo = new ContainerInfo();
            containerInfo.setContainerId(container.getId());
            containerInfo.setImage(container.getImage());
            containerInfo.setRunning(container.getState().equals("running"));
            containerInfo.setWorkerRegion(
                    Utils.queueNameToRegion.get(
                            Arrays.stream(container.getCommand().split(" "))
                                    .reduce((first, second) -> second)
                                    .orElseThrow()
                    )
            );
            containerInfoList.add(containerInfo);
        }

        return containerInfoList;
    }

    public void stopWorker(String containerId) throws ContainerNotFoundException {
        try {
            this.dockerClient.stopContainerCmd(containerId).exec();
            this.dockerClient.removeContainerCmd(containerId).exec();
        } catch (NotFoundException ex) {
            throw new ContainerNotFoundException("Could not find given container with ID [" + containerId + "]", ex);
        }
    }

    public Boolean imageExists(String imageName) {
        for (Image image :
                this.dockerClient.listImagesCmd().exec()) {
            if (Arrays.stream(image.getRepoTags())
                    .anyMatch(tag ->
                            tag.split(":")[0]
                                    .equals(imageName)))
                return true;
        }
        return false;
    }

    public ContainerInfo startWorker(Region region, String imageName) throws ContainerException {
        String cid;
        try {
            cid = this.dockerClient.createContainerCmd(imageName).withCmd(Utils.regionToQueueName.get(region)).exec().getId();
        } catch (NotFoundException ex) {
            throw new ContainerException("Failed to start container", ex);
        }
        if (cid == null) throw new ContainerException("Failed to start container");
        this.dockerClient.startContainerCmd(cid).exec();
        InspectContainerResponse containerResponse = this.dockerClient.inspectContainerCmd(cid).exec();
        if (containerResponse == null) throw new ContainerException("No response from container");
        ContainerInfo containerInfo = new ContainerInfo();
        containerInfo.setWorkerRegion(region);
        containerInfo.setImage(Constants.WORKER_IMAGE_NAME);
        containerInfo.setContainerId(cid);
        InspectContainerResponse.ContainerState state = containerResponse.getState();
        if (state == null)
            throw new ContainerException("Failed to get state from container, container may already be running");
        Boolean running = state.getRunning();
        if (running == null)
            throw new ContainerException("Failed to get state from container, container may already be running");
        containerInfo.setRunning(running);
        return containerInfo;
    }

    @Override
    public void close() throws IOException {
        if (this.dockerClient != null) {
            this.dockerClient.close();
        }
    }
}
