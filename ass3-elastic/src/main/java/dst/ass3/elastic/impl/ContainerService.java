package dst.ass3.elastic.impl;

import dst.ass3.elastic.*;
import dst.ass3.messaging.Region;

import java.io.IOException;
import java.util.List;

public class ContainerService implements IContainerService {

    /**
     * Returns a list of all running containers.
     *
     * @return a list of ContainerInfo objects
     * @throws ContainerException if an error occurred when trying to fetch the running containers.
     */
    @Override
    public List<ContainerInfo> listContainers() throws ContainerException {
        try (DockerClientResource dockerClient = new DockerClientResource(Constants.DOCKER_URL)) {
            return dockerClient.listContainers();
        } catch (Exception ex) {
            throw new ContainerException("Failed to fetch running containers");
        }
    }

    /**
     * Stops the container with the given container ID.
     *
     * @param containerId ID of the container to stop.
     * @throws ContainerNotFoundException if the container to stop is not running
     * @throws ContainerException         if another error occurred when trying to stop the container
     */
    @Override
    public void stopContainer(String containerId) throws ContainerException {
        try (DockerClientResource dockerClient = new DockerClientResource(Constants.DOCKER_URL)) {
            dockerClient.stopWorker(containerId);
        } catch (IOException ex) {
            throw new ContainerException("Failed to stop container");
        }
    }

    /**
     * Starts a worker for the specific {@link Region}.
     *
     * @param region {@link Region} of the worker to start
     * @return ContainerInfo of the started container / worker
     * @throws ImageNotFoundException if the worker docker image is not available
     * @throws ContainerException     if another error occurred when trying to start the worker
     */
    @Override
    public ContainerInfo startWorker(Region region) throws ContainerException {
        try (DockerClientResource dockerClient = new DockerClientResource(Constants.DOCKER_URL)) {
            if (!dockerClient.imageExists(Constants.WORKER_IMAGE_NAME))
                throw new ImageNotFoundException("Failed to find image " + Constants.WORKER_IMAGE_NAME + "!");
            return dockerClient.startWorker(region, Constants.WORKER_IMAGE_NAME);
        } catch (IOException ex) {
            throw new ContainerException("Failed to start container");
        }

    }
}
