package dst.ass3.elastic.impl;

import dst.ass3.elastic.ContainerException;
import dst.ass3.elastic.ContainerInfo;
import dst.ass3.elastic.IContainerService;
import dst.ass3.elastic.IElasticityController;
import dst.ass3.messaging.IWorkloadMonitor;
import dst.ass3.messaging.Region;

import java.util.List;
import java.util.stream.Collectors;

public class ElasticityController implements IElasticityController {

    private final IContainerService containerService;
    private final IWorkloadMonitor workloadMonitor;

    public ElasticityController(IContainerService containerService, IWorkloadMonitor workloadMonitor) {
        this.containerService = containerService;
        this.workloadMonitor = workloadMonitor;
    }

    @Override
    public void adjustWorkers() throws ContainerException {
        for (Region region :
                Region.values()) {
            adjustRegion(region);
        }
    }

    private void adjustRegion(Region region) throws ContainerException {
        long k = workloadMonitor.getWorkerCount().get(region);
        long q = workloadMonitor.getRequestCount().get(region);
        double r_hat = workloadMonitor.getAverageProcessingTime().get(region);
        long r_max = Utils.regionToMaxWaitTime.get(region);
        double r_exp = q * r_hat / k;
        long rel = (long) Math.ceil(q * r_hat / r_max);  // relative amount of average time per queued reqest to r_max

        if (r_exp > r_max * (1 + Constants.SCALE_OUT_THRESHOLD)) { // exceeding the scale out threshold, scaling up
            // with this enough containers should be there to take over the new requests and push r_exp to r_max
            for (long i = 0; i < rel - k; i++) {
                containerService.startWorker(region);
            }
        } else if (r_exp < r_max * (1 - Constants.SCALE_DOWN_THRESHOLD)) { // exceeding the scale down threshold, scaling down
            // stopped containers should scale down enough so that r_exp approximately is r_max
            List<String> to_stop = containerService.listContainers()
                    .stream()
                    .filter(containerInfo -> containerInfo.getWorkerRegion().equals(region))
                    .limit(k - rel)
                    .map(ContainerInfo::getContainerId)
                    .collect(Collectors.toList());
            // gotta to this manually, thanks Oracle for messing up checked exceptions inside foreach streams
            // https://stackoverflow.com/a/27668305
            for (String id :
                    to_stop) {
                containerService.stopContainer(id);
            }
        }
    }
}
