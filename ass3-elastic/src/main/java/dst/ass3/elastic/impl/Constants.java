package dst.ass3.elastic.impl;

public final class Constants {

    public static final String DOCKER_HOST = "192.168.99.99";
    public static final String DOCKER_PORT = "2375";
    public static final String DOCKER_URL = "tcp://" + DOCKER_HOST + ":" + DOCKER_PORT;

    public static final String WORKER_IMAGE_NAME = "dst/ass3-worker";

    public static final long MAX_TIME_AT_VIENNA = 30 * 1000;
    public static final long MAX_TIME_AT_LINZ = 30 * 1000;
    public static final long MAX_TIME_DE_BERLIN = 120 * 1000;

    public static final double SCALE_OUT_THRESHOLD = 0.1;
    public static final double SCALE_DOWN_THRESHOLD = 0.05;
}
