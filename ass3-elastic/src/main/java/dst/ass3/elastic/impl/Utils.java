package dst.ass3.elastic.impl;

import dst.ass3.messaging.Region;

import java.util.HashMap;
import java.util.Map;

public class Utils {
    public final static HashMap<Region, String> regionToQueueName = new HashMap<>();
    public final static HashMap<String, Region> queueNameToRegion = new HashMap<>();
    public final static HashMap<Region, Long> regionToMaxWaitTime = new HashMap<>();

    static {
        regionToQueueName.put(Region.AT_VIENNA, getQueueName(dst.ass3.messaging.Constants.QUEUE_AT_VIENNA));
        regionToQueueName.put(Region.AT_LINZ, getQueueName(dst.ass3.messaging.Constants.QUEUE_AT_LINZ));
        regionToQueueName.put(Region.DE_BERLIN, getQueueName(dst.ass3.messaging.Constants.QUEUE_DE_BERLIN));

        for (Map.Entry<Region, String> entry : regionToQueueName.entrySet()) {
            queueNameToRegion.put(entry.getValue(), entry.getKey());
        }

        regionToMaxWaitTime.put(Region.AT_VIENNA, Constants.MAX_TIME_AT_VIENNA);
        regionToMaxWaitTime.put(Region.AT_LINZ, Constants.MAX_TIME_AT_LINZ);
        regionToMaxWaitTime.put(Region.DE_BERLIN, Constants.MAX_TIME_DE_BERLIN);
    }

    private static String getQueueName(String queue) {
        return queue.replace("dst.", "");
    }
}
