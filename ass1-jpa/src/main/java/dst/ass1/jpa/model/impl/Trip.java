package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import dst.ass1.jpa.util.Constants;

import javax.persistence.Table;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Table(name= Constants.T_TRIP)
public class Trip implements ITrip {
    protected Long id;
    protected Date created;
    protected Date updated;
    protected TripState state;
    protected ILocation pickup;
    protected ILocation destination;
    protected Collection<ILocation> stops;
    protected IMatch match;
    protected ITripInfo tripInfo;
    protected IRider rider;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public Date getUpdated() {
        return this.updated;
    }

    @Override
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public TripState getState() {
        return state;
    }

    @Override
    public void setState(TripState state) {
        this.state = state;
    }

    @Override
    public ILocation getPickup() {
        return pickup;
    }

    @Override
    public void setPickup(ILocation pickup) {
        this.pickup = pickup;
    }

    @Override
    public ILocation getDestination() {
        return destination;
    }

    @Override
    public void setDestination(ILocation destination) {
        this.destination = destination;
    }

    @Override
    public Collection<ILocation> getStops() {
        return stops;
    }

    @Override
    public void setStops(Collection<ILocation> stops) {
        this.stops = stops;
    }

    @Override
    public void addStop(ILocation stop) {
        this.stops.add(stop);
    }

    @Override
    public ITripInfo getTripInfo() {
        return this.tripInfo;
    }

    @Override
    public void setTripInfo(ITripInfo tripInfo) {
        this.tripInfo = tripInfo;
    }

    @Override
    public IMatch getMatch() {
        return this.match;
    }

    @Override
    public void setMatch(IMatch match) {
        this.match = match;
    }

    @Override
    public IRider getRider() {
        return this.rider;
    }

    @Override
    public void setRider(IRider rider) {
        this.rider = rider;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trip trip = (Trip) o;
        return Objects.equals(getId(), trip.getId());
    }
}
