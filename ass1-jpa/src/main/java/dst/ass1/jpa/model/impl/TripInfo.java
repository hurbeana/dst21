package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IMoney;
import dst.ass1.jpa.model.ITrip;
import dst.ass1.jpa.model.ITripInfo;
import dst.ass1.jpa.util.Constants;
import org.hibernate.annotations.Target;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name= Constants.T_TRIP_INFO)
@NamedQuery(
        name=Constants.Q_SUM_DISTANCE_MOST_RECENT_TRIP,
        query = "SELECT sum(ti.distance) FROM TripInfo ti, " +
                "IN (ti.trip) AS t, " +
                "IN (t.rider) AS r " +
                "WHERE r IN " +
                "(SELECT rBack FROM Rider rBack, " +
                "IN (rBack.trips) as tBack, " +
                "IN (tBack.tripInfo) as tiBack " +
                "WHERE tiBack.completed IN " +
                "(SELECT max(tiMax.completed) from TripInfo tiMax)) " +
                "GROUP BY r"
)
public class TripInfo implements ITripInfo {
    @Id
    @GeneratedValue
    protected Long id;

    @Column(name=Constants.M_TRIP_INFO_COMPLETED)
    protected Date completed;

    @Column(name=Constants.M_TRIP_INFO_DISTANCE)
    protected Double distance;

    @Column(name=Constants.M_TRIP_INFO_TOTAL)
    @Embedded
    @Target(Money.class)
    protected IMoney total;

    @Column(name=Constants.M_TRIP_INFO_DRIVER_RATING)
    protected Integer driverRating;

    @Column(name=Constants.M_TRIP_INFO_RIDER_RATING)
    protected Integer riderRating;

    @OneToOne(targetEntity = Trip.class)
    @JoinColumn(nullable = false)
    protected ITrip trip;

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Date getCompleted() {
        return this.completed;
    }

    @Override
    public void setCompleted(Date date) {
        this.completed = date;
    }

    @Override
    public Double getDistance() {
        return this.distance;
    }

    @Override
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public IMoney getTotal() {
        return this.total;
    }

    @Override
    public void setTotal(IMoney money) {
        this.total = (Money) money;
    }

    @Override
    public Integer getDriverRating() {
        return this.driverRating;
    }

    @Override
    public void setDriverRating(Integer driverRating) {
        this.driverRating = driverRating;
    }

    @Override
    public Integer getRiderRating() {
        return this.driverRating;
    }

    @Override
    public void setRiderRating(Integer riderRating) {
        this.riderRating = riderRating;
    }

    @Override
    public ITrip getTrip() {
        return this.trip;
    }

    @Override
    public void setTrip(ITrip trip) {
        this.trip = trip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripInfo tripInfo = (TripInfo) o;
        return Objects.equals(getId(), tripInfo.getId());
    }
}
