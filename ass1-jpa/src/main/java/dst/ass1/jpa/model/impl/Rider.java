package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IPreferences;
import dst.ass1.jpa.model.IRider;
import dst.ass1.jpa.model.ITrip;
import dst.ass1.jpa.util.Constants;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(
        name=Constants.T_RIDER,
        uniqueConstraints={
            @UniqueConstraint(columnNames = {Constants.M_RIDER_ACCOUNT, Constants.M_RIDER_BANK_CODE})
        }
)
@NamedQuery(
        name=Constants.Q_RIDER_BY_EMAIL,
        query="SELECT r FROM Rider r WHERE r.email = :email"
)
public class Rider extends PlatformUser implements IRider {
    @Column(unique = true, nullable = false, name=Constants.M_RIDER_EMAIL)
    protected String email;

    @Column(length = 20, name=Constants.M_RIDER_PASSWORD)
    protected byte[] password;

    @Column(name=Constants.M_RIDER_ACCOUNT)
    protected String accountNo;
    @Column(name=Constants.M_RIDER_BANK_CODE)
    protected String bankCode;

    @OneToMany(targetEntity = Trip.class, fetch = FetchType.EAGER, mappedBy = "rider")
    @NotFound(action = NotFoundAction.IGNORE)
    private Collection<ITrip> trips;

    @OneToOne(targetEntity = Preferences.class, cascade = CascadeType.REMOVE)
    @JoinColumn(nullable = false, unique = true)
    protected IPreferences preferences;

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getTel() {
        return this.tel;
    }

    @Override
    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public Double getAvgRating() {
        return this.avgRating;
    }

    @Override
    public void setAvgRating(Double avgRating) {
        this.avgRating = avgRating;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public byte[] getPassword() {
        return this.password;
    }

    @Override
    public void setPassword(byte[] password) {
        this.password = password;
    }

    @Override
    public String getAccountNo() {
        return this.accountNo;
    }

    @Override
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    @Override
    public String getBankCode() {
        return this.bankCode;
    }

    @Override
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Override
    public IPreferences getPreferences() {
        return this.preferences;
    }

    @Override
    public void setPreferences(IPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public Collection<ITrip> getTrips() {
        return this.trips;
    }

    @Override
    public void setTrips(Collection<ITrip> trips) {
        this.trips = trips;
    }

    @Override
    public void addTrip(ITrip trip) {
        this.trips.add(trip);
    }


}
