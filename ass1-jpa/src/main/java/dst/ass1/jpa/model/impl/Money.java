package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IMoney;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Embeddable
public class Money implements IMoney {
    protected String currency;
    protected BigDecimal value;

    public Money() {}
    public Money(String currency, BigDecimal value) {
        this.setCurrency(currency);
        this.setValue(value);
    }

    @Override
    public String getCurrency() {
        return this.currency;
    }

    @Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public BigDecimal getValue() {
        return this.value;
    }

    @Override
    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
