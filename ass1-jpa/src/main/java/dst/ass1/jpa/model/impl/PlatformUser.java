package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IPlatformUser;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

// Polymorphic queries (e.g. getting all Platform Users is not possible)
@MappedSuperclass
public abstract class PlatformUser implements IPlatformUser {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;
    @Column(nullable = false)
    protected String tel;
    protected Double avgRating;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlatformUser that = (PlatformUser) o;
        return Objects.equals(getId(), that.getId());
    }
}
