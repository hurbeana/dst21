package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import dst.ass1.jpa.util.Constants;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Target;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name=Constants.T_MATCH)
public class Match implements IMatch {
    @Id
    @GeneratedValue()
    protected Long id;
    @Temporal(TemporalType.DATE)
    @Column(name=Constants.M_MATCH_DATE)
    protected Date date;
    @Column(name=Constants.M_MATCH_FARE)
    @Target(Money.class)
    protected IMoney fare;
    @ManyToOne(targetEntity = Trip.class)
    @NotFound(action = NotFoundAction.IGNORE)
    protected ITrip trip;
    @ManyToOne(targetEntity = Vehicle.class)
    protected IVehicle vehicle;
    @ManyToOne(targetEntity = Driver.class)
    protected IDriver driver;

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Date getDate() {
        return this.date;
    }

    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public IMoney getFare() {
        return this.fare;
    }

    @Override
    public void setFare(IMoney money) {
        this.fare = money;
    }

    @Override
    public ITrip getTrip() {
        return this.trip;
    }

    @Override
    public void setTrip(ITrip trip) {
        this.trip = trip;
    }

    @Override
    public IVehicle getVehicle() {
        return this.vehicle;
    }

    @Override
    public void setVehicle(IVehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public IDriver getDriver() {
        return this.driver;
    }

    @Override
    public void setDriver(IDriver driver) {
        this.driver = driver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Match match = (Match) o;
        return Objects.equals(getId(), match.getId());
    }
}
