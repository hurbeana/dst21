package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IVehicle;
import dst.ass1.jpa.util.Constants;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name= Constants.T_VEHICLE)
public class Vehicle implements IVehicle {
    @Id
    @GeneratedValue
    protected Long id;
    @Column(name=Constants.M_VEHICLE_LICENSE, unique = true)
    protected String license;
    @Column(name=Constants.M_VEHICLE_COLOR)
    protected String color;
    @Column(name=Constants.M_VEHICLE_TYPE)
    protected String type;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getLicense() {
        return this.license;
    }

    @Override
    public void setLicense(String license) {
        this.license = license;
    }

    @Override
    public String getColor() {
        return this.color;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return Objects.equals(getId(), vehicle.getId());
    }
}
