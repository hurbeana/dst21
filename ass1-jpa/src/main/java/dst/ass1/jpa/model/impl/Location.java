package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.ILocation;
import dst.ass1.jpa.util.Constants;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name=Constants.T_LOCATION)
public class Location implements ILocation {
    @Id
    @GeneratedValue()
    protected Long id;
    @Column(name= Constants.M_LOCATION_NAME)
    protected String name;
    @Column(name=Constants.M_LOCATION_LOCATION_ID)
    protected Long locationId;

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getLocationId() {
        return this.locationId;
    }

    @Override
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(this.id, location.id);
    }
}
