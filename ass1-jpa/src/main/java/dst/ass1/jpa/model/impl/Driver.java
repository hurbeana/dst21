package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import dst.ass1.jpa.util.Constants;
import org.hibernate.annotations.Target;

import javax.persistence.*;

import java.util.*;

@Entity
@Table(name=Constants.T_DRIVER)
@NamedQuery(
        name=Constants.Q_ACTIVE_IN_MULITIPLE_ORGANIZATIONS_DRIVERS,
        query="SELECT d from Driver d, IN (d.employments) AS e " +
                "WHERE function('DATEDIFF', 'M', e.since, function('TODAY')) >= 1 " +
                "GROUP BY d.id " +
                "HAVING COUNT(e) > :organizations"
)
public class Driver extends PlatformUser implements IDriver {
    @ManyToOne(targetEntity = Vehicle.class)
    @JoinColumn(nullable = false)
    @Target(Vehicle.class)
    protected IVehicle vehicle;
    @OneToMany(targetEntity = Employment.class)
    protected Collection<IEmployment> employments = new ArrayList<>();

    @Override
    public Collection<IEmployment> getEmployments() {
        return this.employments;
    }

    @Override
    public void setEmployments(Collection<IEmployment> employments) {
        this.employments = new HashSet<>(employments);
    }

    @Override
    public void addEmployment(IEmployment employment) {
        this.employments.add(employment);
    }

    @Override
    public IVehicle getVehicle() {
        return this.vehicle;
    }

    @Override
    public void setVehicle(IVehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getTel() {
        return this.tel;
    }

    @Override
    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public Double getAvgRating() {
        return this.avgRating;
    }

    @Override
    public void setAvgRating(Double avgRating) {
        this.avgRating = avgRating;
    }
}
