package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IEmployment;
import dst.ass1.jpa.model.IEmploymentKey;
import dst.ass1.jpa.util.Constants;
import org.hibernate.annotations.Target;

import javax.persistence.*;

import java.util.Date;
import java.util.Objects;

@Entity
@Table(name=Constants.T_EMPLOYMENT)
public class Employment implements IEmployment {
    @EmbeddedId
    @Target(EmploymentKey.class)
    protected IEmploymentKey id;

    @Column(name=Constants.M_EMPLOYMENT_SINCE)
    @Temporal(TemporalType.DATE)
    protected Date since;

    @Column(name=Constants.M_EMPLOYMENT_ACTIVE)
    protected Boolean active;

    @Override
    public IEmploymentKey getId() {
        return this.id;
    }

    @Override
    public void setId(IEmploymentKey employmentKey) {
        this.id = employmentKey;
    }

    @Override
    public Date getSince() {
        return this.since;
    }

    @Override
    public void setSince(Date since) {
        this.since = since;
    }

    @Override
    public Boolean isActive() {
        return this.active;
    }

    @Override
    public void setActive(Boolean active) {
        this.active = active;
    }

}
