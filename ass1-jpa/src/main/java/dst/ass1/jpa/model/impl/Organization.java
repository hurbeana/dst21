package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IEmployment;
import dst.ass1.jpa.model.IOrganization;
import dst.ass1.jpa.model.IVehicle;
import dst.ass1.jpa.util.Constants;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name=Constants.T_ORGANIZATION)
public class Organization implements IOrganization {
    @Id
    @GeneratedValue
    @Column(name=Constants.I_ORGANIZATION)
    protected Long id;
    @Column(name=Constants.M_ORGANIZATION_NAME)
    protected String name;

    @ManyToMany(targetEntity = Organization.class)
    @JoinTable(name=Constants.J_ORGANIZATION_PARTS,
            inverseJoinColumns = @JoinColumn(name=Constants.I_ORGANIZATION_PART_OF),
            joinColumns = @JoinColumn(name=Constants.I_ORGANIZATION_PARTS)
    )
    protected Collection<IOrganization> parts = new ArrayList<>();

    @ManyToMany(targetEntity = Organization.class, mappedBy = Constants.M_ORGANIZATION_PARTS)
    protected Collection<IOrganization> partOf = new ArrayList<>();

    @OneToMany(targetEntity = Employment.class)
    protected Collection<IEmployment> employments = new ArrayList<>();

    @ManyToMany(targetEntity = Vehicle.class)
    @JoinTable(
            name=Constants.J_ORGANIZATION_VEHICLE,
            joinColumns = @JoinColumn(name=Constants.I_ORGANIZATION),
            inverseJoinColumns = @JoinColumn(name=Constants.I_VEHICLES)
    )
    protected Collection<IVehicle> vehicles;

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Collection<IOrganization> getParts() {
        return this.parts;
        //return null;
    }

    @Override
    public void setParts(Collection<IOrganization> parts) {
        this.parts = parts;
    }

    @Override
    public void addPart(IOrganization part) {
        this.parts.add(part);
    }

    @Override
    public Collection<IOrganization> getPartOf() {
        return this.partOf;
        //return null;
    }

    @Override
    public void setPartOf(Collection<IOrganization> partOf) {
        this.partOf = partOf;
    }

    @Override
    public void addPartOf(IOrganization partOf) {
        this.partOf.add(partOf);
    }

    @Override
    public Collection<IEmployment> getEmployments() {
        return this.employments;
    }

    @Override
    public void setEmployments(Collection<IEmployment> employments) {
        this.employments = employments;
    }

    @Override
    public void addEmployment(IEmployment employment) {
        this.employments.add(employment);
    }

    @Override
    public Collection<IVehicle> getVehicles() {
        return this.vehicles;
    }

    @Override
    public void setVehicles(Collection<IVehicle> vehicles) {
        this.vehicles = vehicles;
    }

    @Override
    public void addVehicle(IVehicle vehicle) {
        this.vehicles.add(vehicle);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Organization that = (Organization) o;
        return Objects.equals(getId(), that.getId());
    }
}
