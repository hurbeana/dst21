package dst.ass1.jpa.interceptor;

import org.hibernate.EmptyInterceptor;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class SQLInterceptor extends EmptyInterceptor {

    private static final long serialVersionUID = -3082243834965597947L;
    private static final String sql_pattern = "(?i).*SELECT .* FROM (Trip|Location).*";
    private static final AtomicInteger count = new AtomicInteger(0);
    private static final AtomicBoolean isVerbose = new AtomicBoolean(false);

    public static void resetCounter() {
        count.set(0);
    }

    public static int getSelectCount() {
        return count.get();
    }

    /**
     * If the verbose argument is set, the interceptor prints the intercepted SQL statements to System.out.
     *
     * @param verbose whether or not to be verbose
     */
    public static void setVerbose(boolean verbose) {
        isVerbose.set(verbose);
    }

    @Override
    public String onPrepareStatement(String sql) {
        if(isVerbose.get()) System.out.println(sql);
        if(Pattern.matches(sql_pattern, sql)) count.incrementAndGet();
        return sql;
    }

}
