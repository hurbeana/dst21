package dst.ass1.jpa.listener;

import dst.ass1.jpa.model.impl.Trip;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

public class TripListener {

   @PrePersist
    void onPrePersist(Trip trip) {
       Date now = new Date();
       trip.setCreated(now);
       trip.setUpdated(now);
    }

    @PreUpdate
    void onPreUpdate(Trip trip) {
        trip.setUpdated(new Date());
    }
}
