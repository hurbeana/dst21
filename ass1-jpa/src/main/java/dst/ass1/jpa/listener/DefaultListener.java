package dst.ass1.jpa.listener;


import javax.persistence.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class DefaultListener {
    private static final ConcurrentHashMap<Object, Recording> persist = new ConcurrentHashMap<>();
    private static final AtomicInteger updated = new AtomicInteger(0);
    private static final AtomicInteger removed = new AtomicInteger(0);
    private static final AtomicInteger loaded = new AtomicInteger(0);

    @PrePersist
    private static void onPrePersist(Object o) {
        persist.put(o, new Recording());
    }

    @PostPersist
    private static void onPostPersist(Object o) {
        persist.put(o, persist.get(o).finish());
    }

    @PostLoad
    private static void onPostLoad(Object o) {
        loaded.incrementAndGet();
    }

    @PostUpdate
    private static void onPostUpdate(Object o) {
        updated.incrementAndGet();
    }

    @PostRemove
    private static void onPostRemove(Object o) {
        removed.incrementAndGet();
    }

    public static int getLoadOperations() {
        return loaded.get();
    }

    public static int getUpdateOperations() {
        return updated.get();
    }

    public static int getRemoveOperations() {
        return removed.get();
    }

    public static int getPersistOperations() {
        return persist.size();
    }

    public static long getOverallTimeToPersist() {
        return persist.values().stream()
                .map(Recording::getDuration)
                .mapToLong(Long::longValue)
                .sum();
    }

    public static double getAverageTimeToPersist() {
        return persist.values().stream()
                .map(Recording::getDuration)
                .mapToLong(Long::longValue)
                .average().orElse(0);
    }

    /**
     * Clears the internal data structures that are used for storing the operations.
     */
    public static void clear() {
        loaded.set(0);
        updated.set(0);
        removed.set(0);
        persist.clear();
    }

    private static class Recording{
        private final long begin;
        private long end;

        public Recording() {
            this.begin = System.currentTimeMillis();
            this.end = -1;
        }

        public Recording finish() {
            if(this.end >= 0) return this;
            this.end = System.currentTimeMillis();
            return this;
        }

        public long getDuration() {
            return this.end - this.begin;
        }
    }
}
