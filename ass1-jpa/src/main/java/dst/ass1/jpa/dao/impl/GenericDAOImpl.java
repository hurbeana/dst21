package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.GenericDAO;

import javax.persistence.EntityManager;
//import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class GenericDAOImpl<T> implements GenericDAO<T> {
    private final Class<? extends T> type;
    protected EntityManager em;

    public GenericDAOImpl(Class<? extends T> type, EntityManager em) {
        this.em = em;
        // doesn't work with reflection because it resolves to the interface level
        //this.type = (Class<? extends T>) ((ParameterizedType) getClass()
        //        .getGenericSuperclass()).getActualTypeArguments()[0];
        this.type = type;
    }

    public T findById(Long id) {
        return em.find(this.type, id);
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        String q = String.format("SELECT t from %s t", this.type.getName());
        return (List<T>) em.createQuery(q, this.type).getResultList();
    }
}
