package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IMatchDAO;
import dst.ass1.jpa.model.IMatch;
import dst.ass1.jpa.model.impl.Match;

import javax.persistence.EntityManager;
import java.util.List;

public class MatchDAO extends GenericDAOImpl<IMatch> implements IMatchDAO {
    public MatchDAO(EntityManager em) {
        super(Match.class, em);
    }
}
