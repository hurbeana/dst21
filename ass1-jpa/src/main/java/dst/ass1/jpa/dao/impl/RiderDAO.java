package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IRiderDAO;
import dst.ass1.jpa.model.IMoney;
import dst.ass1.jpa.model.IRider;
import dst.ass1.jpa.model.impl.Money;
import dst.ass1.jpa.model.impl.Rider;
import dst.ass1.jpa.model.impl.TripInfo;
import dst.ass1.jpa.util.Constants;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

public class RiderDAO extends GenericDAOImpl<IRider> implements IRiderDAO {

    public RiderDAO(EntityManager em) {
        super(Rider.class, em);
    }

    @Override
    public IRider findByEmail(String email) {
        try {
            return em
                    .createNamedQuery(Constants.Q_RIDER_BY_EMAIL, Rider.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Double getTotalDistanceOfMostRecentRider() {
        try {
            return em
                    .createNamedQuery(Constants.Q_SUM_DISTANCE_MOST_RECENT_TRIP, Double.class)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Map<IRider, Map<String, IMoney>> getRecentSpending() {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Object[]> q = cb.createQuery(Object[].class);
        Root<Rider> riderRoot = q.from(Rider.class);
        Join<TripInfo,Rider> jTripTripInfo = riderRoot.join(Constants.A_TRIP + "s").join(Constants.A_TRIP_INFO);

        LocalDate beforelocdt = LocalDate.now().minusDays(30);
        Date before30Date = Date.from(beforelocdt.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Predicate before30Pred = cb.greaterThanOrEqualTo(jTripTripInfo.get(Constants.M_TRIP_INFO_COMPLETED),
                before30Date);

        Path<String> currency = jTripTripInfo.get(Constants.M_TRIP_INFO_TOTAL).get("currency");
        Path<BigDecimal> value = jTripTripInfo.get(Constants.M_TRIP_INFO_TOTAL).get("value");

        q.multiselect(riderRoot, currency, cb.sum(value))
                .where(before30Pred).groupBy(riderRoot, currency);

        TypedQuery<Object[]> query = em.createQuery(q);
        Collection<Object[]> col = query.getResultList();
        Map<IRider, Map<String, IMoney>> ret = new HashMap<>();
        for (Object[] res : col)
        {
            Money money = new Money((String)res[1], (BigDecimal)res[2]);
            Rider rider = (Rider)res[0];
            ret.putIfAbsent(rider, new HashMap<>());
            ret.get(rider).put(money.getCurrency(), money);
        }
        return ret;
    }
}
