package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IDriverDAO;
import dst.ass1.jpa.model.IDriver;
import dst.ass1.jpa.model.impl.Driver;
import dst.ass1.jpa.util.Constants;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.List;

public class DriverDAO extends GenericDAOImpl<IDriver> implements IDriverDAO {

    public DriverDAO(EntityManager em) {
        super(Driver.class, em);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<IDriver> findActiveInMultipleOrganizationsDrivers(Long numberOfOrganizations) {
        try{
            return (List<IDriver>)(Object)em.createNamedQuery(Constants.Q_ACTIVE_IN_MULITIPLE_ORGANIZATIONS_DRIVERS, Driver.class)
                    .setParameter("organizations", numberOfOrganizations)
                    .getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }
}
