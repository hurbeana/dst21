package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IOrganizationDAO;
import dst.ass1.jpa.model.IOrganization;
import dst.ass1.jpa.model.impl.Organization;

import javax.persistence.EntityManager;
import java.util.List;

public class OrganizationDAO extends GenericDAOImpl<IOrganization> implements IOrganizationDAO {
    public OrganizationDAO(EntityManager em) {
        super(Organization.class, em);
    }
}
