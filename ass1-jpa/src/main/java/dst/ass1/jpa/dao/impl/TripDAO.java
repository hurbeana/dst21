package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.ITripDAO;
import dst.ass1.jpa.model.ITrip;
import dst.ass1.jpa.model.TripState;
import dst.ass1.jpa.model.impl.Trip;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class TripDAO extends GenericDAOImpl<ITrip> implements ITripDAO {

    public TripDAO(EntityManager em) {
        super(Trip.class, em);
    }

    @Override
    public Collection<ITrip> findCancelledTrips(Date start, Date end) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<ITrip> q = cb.createQuery(ITrip.class);
        Root<Trip> t = q.from(Trip.class);

        Collection<Predicate> pArray = new ArrayList<>();
        pArray.add(cb.equal(t.get("state"), TripState.CANCELLED));

        if(start != null){
            pArray.add(cb.greaterThan(t.get("created"), start));
        }
        if(end != null){
            pArray.add(cb.lessThan(t.get("created"), end));
        }
        Predicate conj = cb.and(pArray.toArray(new Predicate[pArray.size()]));

        return em.createQuery(q.select(t).where(conj)).getResultList();
    }
}
