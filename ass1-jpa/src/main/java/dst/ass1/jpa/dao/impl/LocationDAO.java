package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.ILocationDAO;
import dst.ass1.jpa.model.ILocation;
import dst.ass1.jpa.model.impl.Location;
import dst.ass1.jpa.util.Constants;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LocationDAO extends GenericDAOImpl<ILocation> implements ILocationDAO {

    public LocationDAO(EntityManager em) {
        super(Location.class, em);
    }

    @Override
    public Collection<Long> findReachedLocationIds() {
        try {
            // Slow querying by doing n+1 selects
            // SELECT * FROM Cars;
            // then
            // SELECT * FROM Wheel WHERE CardId = ?
            // is slow because youre doing N additional selects (N = total number of cars) + 1 to get the cars
            // Another option is to do fetching data into memory beforehand
            // List departments = entityManager.createQuery("select d from
            // Department d left join fetch d.employees").getResultList();
             Collection<Long> destinations = em
                     .createNamedQuery(Constants.Q_REACHED_LOCATIONS, Long.class)
                     .getResultList();
            Collection<Long> stops = em
                    .createNamedQuery("reachedStops", Long.class)
                    .getResultList();
            Set<Long> distinct = new HashSet<>();
            distinct.addAll(destinations);
            distinct.addAll(stops);
            return distinct;
        } catch (NoResultException e) {
            return null;
        }
    }
}
