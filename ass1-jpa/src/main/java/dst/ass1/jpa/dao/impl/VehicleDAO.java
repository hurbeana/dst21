package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IVehicleDAO;
import dst.ass1.jpa.model.IVehicle;
import dst.ass1.jpa.model.impl.Vehicle;

import javax.persistence.EntityManager;
import java.util.List;

public class VehicleDAO extends GenericDAOImpl<IVehicle> implements IVehicleDAO {
    public VehicleDAO(EntityManager em) {
        super(Vehicle.class, em);
    }
}
