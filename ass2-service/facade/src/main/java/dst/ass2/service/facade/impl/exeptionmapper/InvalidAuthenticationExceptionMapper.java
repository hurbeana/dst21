package dst.ass2.service.facade.impl.exeptionmapper;

import dst.ass2.service.facade.impl.InvalidAuthenticationException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InvalidAuthenticationExceptionMapper implements ExceptionMapper<InvalidAuthenticationException> {
    @Override
    public Response toResponse(InvalidAuthenticationException e) {
        return Response.status(Response.Status.FORBIDDEN).build();
    }
}
