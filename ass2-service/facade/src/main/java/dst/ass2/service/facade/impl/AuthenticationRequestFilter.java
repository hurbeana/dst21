package dst.ass2.service.facade.impl;

import dst.ass2.service.auth.client.IAuthenticationClient;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;

public class AuthenticationRequestFilter implements ContainerRequestFilter {
    @Inject
    IAuthenticationClient authenticationClient;

    @Override
    public void filter(ContainerRequestContext requestContext) throws InvalidAuthenticationException {
        String tok = requestContext.getHeaderString("Authorization");
        if (tok == null || !tok.contains("Bearer") || !authenticationClient.isTokenValid(tok.replace("Bearer ", "")))
            throw new InvalidAuthenticationException();
    }
}
