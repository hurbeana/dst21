package dst.ass2.service.facade.impl;

import dst.ass2.service.api.trip.*;
import dst.ass2.service.api.trip.rest.ITripServiceResource;
import org.glassfish.jersey.client.proxy.WebResourceFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.net.URI;

@Authorized
@Path("/trips")
public class TripServiceResource implements ITripServiceResource {

    @Inject
    public URI tripServiceURI;

    private ITripServiceResource proxiedResource;

    @PostConstruct
    private void setup() {
        proxiedResource = WebResourceFactory.newResource(ITripServiceResource.class, ClientBuilder.newClient().target(tripServiceURI));
    }

    @Override
    public Response createTrip(Long riderId, Long pickupId, Long destinationId) throws EntityNotFoundException, InvalidTripException {
        return proxiedResource.createTrip(riderId, pickupId, destinationId);
    }

    @Override
    public Response confirm(Long tripId) throws EntityNotFoundException, InvalidTripException {
        return proxiedResource.confirm(tripId);
    }

    @Override
    public Response getTrip(Long tripId) throws EntityNotFoundException {
        return proxiedResource.getTrip(tripId);
    }

    @Override
    public Response deleteTrip(Long tripId) throws EntityNotFoundException {
        return proxiedResource.deleteTrip(tripId);
    }

    @Override
    public Response addStop(Long tripId, Long locationId) throws InvalidTripException, EntityNotFoundException {
        return proxiedResource.addStop(tripId, locationId);
    }

    @Override
    public Response removeStop(Long tripId, Long locationId) throws InvalidTripException, EntityNotFoundException {
        return proxiedResource.removeStop(tripId, locationId);
    }

    @Override
    public Response match(Long tripId, MatchDTO matchDTO) throws EntityNotFoundException, DriverNotAvailableException {
        return proxiedResource.match(tripId, matchDTO);
    }

    @Override
    public Response complete(Long tripId, TripInfoDTO tripInfoDTO) throws EntityNotFoundException {
        return proxiedResource.complete(tripId, tripInfoDTO);
    }

    @Override
    public Response cancel(Long tripId) throws EntityNotFoundException {
        return proxiedResource.cancel(tripId);
    }
}
