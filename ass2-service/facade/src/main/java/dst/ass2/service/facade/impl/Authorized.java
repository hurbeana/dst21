package dst.ass2.service.facade.impl;

import javax.ws.rs.NameBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorized {}

// https://stackoverflow.com/questions/38520881/using-name-binding-annotations-in-jersey