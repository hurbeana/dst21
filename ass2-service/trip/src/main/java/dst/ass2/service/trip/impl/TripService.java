package dst.ass2.service.trip.impl;

import dst.ass1.jpa.dao.*;
import dst.ass1.jpa.model.*;
import dst.ass1.jpa.model.impl.Match;
import dst.ass1.jpa.model.impl.Money;
import dst.ass1.jpa.model.impl.TripInfo;
import dst.ass2.service.api.match.IMatchingService;
import dst.ass2.service.api.trip.*;
import dst.ass2.service.api.trip.EntityNotFoundException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

// https://stackoverflow.com/questions/9921324/javax-faces-bean-managedproperty-in-cdi-named-bean-returns-null/9921449#9921449
// https://stackoverflow.com/questions/10994158/difference-between-named-and-managedbean-annotations-in-jsf2-0-tomcat7
@Named
// The bean has the @ApplicationScoped annotation to specify that its context extends for the duration of the user’s interaction with the application
// This is basically so that the container supplies always the same TripService throughout its livetime
@ApplicationScoped
@Transactional
public class TripService implements ITripService {

    @Inject
    IModelFactory modelFactory;

    @Inject
    IDAOFactory daoFactory;

    @Inject
    IMatchingService matchingService;

    @PersistenceContext
    private EntityManager em;

    private ITripDAO tripDAO;
    private IRiderDAO riderDAO;
    private IDriverDAO driverDAO;
    private IVehicleDAO vehicleDAO;
    private ILocationDAO locationDAO;

    @PostConstruct
    public void setup() {
        tripDAO = daoFactory.createTripDAO();
        riderDAO = daoFactory.createRiderDAO();
        driverDAO = daoFactory.createDriverDAO();
        vehicleDAO = daoFactory.createVehicleDAO();
        locationDAO = daoFactory.createLocationDAO();
    }

    @Override
    public TripDTO create(Long riderId, Long pickupId, Long destinationId) throws EntityNotFoundException {
        ILocation pickup = getLocation(pickupId);
        ILocation dest = getLocation(destinationId);

        IRider rider;
        try {
            rider = riderDAO.findById(riderId);
            if (rider == null) {
                throw new EntityNotFoundException("Could not find rider with id " + riderId);
            }
        } catch (PersistenceException ex) {
            throw new EntityNotFoundException("Could not find rider with id " + riderId);
        }

        ITrip trip = modelFactory.createTrip();
        trip.setDestination(dest);
        trip.setPickup(pickup);
        trip.setRider(rider);
        trip.setState(TripState.CREATED);

        em.persist(trip);

        TripDTO dto = new TripDTO();
        dto.setRiderId(riderId);
        dto.setPickupId(pickupId);
        dto.setDestinationId(destinationId);
        dto.setId(trip.getId());

        // TODO: Is this correct? I still need to implement the other service, don't I?
        try {
            dto.setFare(matchingService.calculateFare(dto));
        } catch (InvalidTripException ex) {
            dto.setFare(null); //just set null if invalid
        }

        return dto;
    }

    @Override
    public void confirm(Long tripId) throws EntityNotFoundException, IllegalStateException, InvalidTripException {
        ITrip trip = getTrip(tripId);

        if(trip.getState() != TripState.CREATED){
            throw new IllegalStateException("The state of the trip has to be CREATED!");
        }

        if(trip.getRider() == null){
            throw new IllegalStateException("The rider cannot be null!");
        }

        trip.setState(TripState.QUEUED);

        em.persist(trip);

        TripDTO dto = new TripDTO();
        dto.setDestinationId(trip.getDestination().getId());
        dto.setPickupId(trip.getPickup().getId());
        dto.setRiderId(trip.getRider().getId());
        dto.setStops(trip.getStops().stream().map(ILocation::getId).collect(Collectors.toList()));
        dto.setFare(matchingService.calculateFare(dto));

        matchingService.queueTripForMatching(tripId);
    }

    @Override
    @Transactional(rollbackOn = {EntityNotFoundException.class, DriverNotAvailableException.class, IllegalStateException.class})
    public void match(Long tripId, MatchDTO match) throws EntityNotFoundException, DriverNotAvailableException, IllegalStateException {
        ITrip trip;
        IDriver driver;
        IVehicle vehicle;

        try{
            trip = getTrip(tripId);
            // lock down the trip so it can't be changed during the tx
            em.lock(trip, LockModeType.PESSIMISTIC_WRITE);
            // check whether the trip is not in the right state
            if(trip.getState() != TripState.QUEUED)
                throw new IllegalStateException("The trip is not in the needed QUEUED state");
            // check whether the matched driver exists (maybe not exists)
            try{
                driver = driverDAO.findById(match.getDriverId());
                if(driver == null)
                    throw new EntityNotFoundException("The driver with id " + match.getDriverId() + " could not be found");
            } catch (PersistenceException e) {
                throw new EntityNotFoundException("The driver with id " + match.getDriverId() + " could not be found");
            }
            // check whether the matched vehicle exists
            try{
                vehicle = vehicleDAO.findById(match.getVehicleId());
                if(vehicle == null)
                    throw new EntityNotFoundException("The vehicle with id " + match.getVehicleId() + " could not be found");
            } catch (PersistenceException e) {
                throw new EntityNotFoundException("The vehicle with id " + match.getVehicleId() + " could not be found");
            }

            // check for rider existence at trip (maybe deleted account in the meantime)
            if(trip.getRider() == null)
                throw new IllegalStateException("The trips rider could not be found in the db during tx");

            // look whether driver is already in another trip
            List<ITrip> trips = tripDAO.findAll();
            for (ITrip checkTrip: trips) {
                IMatch checkTripMatch = checkTrip.getMatch();
                if (checkTripMatch != null) {
                    IDriver checkTripMatchDriver = checkTripMatch.getDriver();
                    if (checkTripMatchDriver != null)
                        if (checkTrip.getState() == TripState.MATCHED
                                || checkTrip.getState() == TripState.APPROACHING
                                || checkTrip.getState() == TripState.IN_PROGRESS)
                            if (checkTripMatchDriver.getId().equals(match.getDriverId()))
                                throw new DriverNotAvailableException("The matched driver with id " + match.getDriverId() + " is unavailable");
                }
            }

            Money fare = new Money();
            MoneyDTO matchFare = match.getFare();
            fare.setCurrency(matchFare.getCurrency());
            fare.setValue(matchFare.getValue());

            Match matchToPersist = new Match();
            matchToPersist.setDate(new Date());
            matchToPersist.setTrip(trip);
            matchToPersist.setDriver(driver);
            matchToPersist.setVehicle(vehicle);
            matchToPersist.setFare(fare);

            trip.setMatch(matchToPersist);
            trip.setState(TripState.MATCHED);

            em.persist(matchToPersist);
            em.persist(trip);
        }
        catch (EntityNotFoundException | DriverNotAvailableException | IllegalStateException | OptimisticLockException e){
            // queue it back
            matchingService.queueTripForMatching(tripId);
            // rethrow
            throw e;
        }

    }

    @Override
    public void complete(Long tripId, TripInfoDTO tripInfoDTO) throws EntityNotFoundException {
        ITrip trip = getTrip(tripId);
        IMoney money = new Money();
        MoneyDTO tripInfoDTOFare = tripInfoDTO.getFare();
        money.setCurrency(tripInfoDTOFare.getCurrency());
        money.setValue(tripInfoDTOFare.getValue());

        ITripInfo tripInfo = new TripInfo();
        tripInfo.setTrip(trip);
        tripInfo.setCompleted(tripInfoDTO.getCompleted());
        tripInfo.setDistance(tripInfoDTO.getDistance());
        tripInfo.setTotal(money);

        trip.setTripInfo(tripInfo);
        trip.setState(TripState.COMPLETED);

        em.persist(tripInfo);
        em.persist(trip);
    }

    @Override
    public void cancel(Long tripId) throws EntityNotFoundException {
        ITrip trip = getTrip(tripId);
        trip.setState(TripState.CANCELLED);
        em.persist(trip);
    }

    @Override
    public boolean addStop(TripDTO trip, Long locationId) throws EntityNotFoundException, IllegalStateException {
        ITrip itrip = getTrip(trip.getId());
        ILocation stop = getLocation(locationId);

        if(itrip.getState() != TripState.CREATED)
            throw new IllegalStateException("Trip is no longer in created state, cannot add location");

        if(itrip.getStops().contains(stop))
            return false;

        itrip.addStop(stop);
        em.persist(itrip);

        trip.getStops().add(locationId);

        try {
            trip.setFare(matchingService.calculateFare(trip));
        } catch (InvalidTripException ex) {
            trip.setFare(null); //just set null if invalid
        }

        return true;
    }

    @Override
    public boolean removeStop(TripDTO trip, Long locationId) throws EntityNotFoundException, IllegalStateException {
        ITrip itrip = getTrip(trip.getId());
        ILocation stop = getLocation(locationId);

        if(itrip.getState() != TripState.CREATED)
            throw new IllegalStateException("Trip is no longer in created state, cannot remove location");

        if(!itrip.getStops().contains(stop))
            return false;

        itrip.getStops().remove(stop);
        em.persist(itrip);

        trip.getStops().remove(locationId);

        try {
            trip.setFare(matchingService.calculateFare(trip));
        } catch (InvalidTripException ex) {
            trip.setFare(null); //just set null if invalid
        }

        return true;
    }

    @Override
    public void delete(Long tripId) throws EntityNotFoundException {
        em.remove(getTrip(tripId));
    }

    @Override
    public TripDTO find(Long tripId) {
        ITrip trip;

        try{
            trip = tripDAO.findById(tripId);
        } catch (PersistenceException ex) {
            return null;
        }
        if (trip == null) {
            return null;
        }

        TripDTO dto = new TripDTO();
        dto.setId(trip.getId());
        dto.setRiderId(trip.getRider().getId());
        dto.setPickupId(trip.getPickup().getId());
        dto.setDestinationId(trip.getDestination().getId());
        dto.setStops(trip.getStops().stream().map(ILocation::getId).collect(Collectors.toList()));

        try {
            dto.setFare(matchingService.calculateFare(dto));
        } catch (InvalidTripException ex) {
            dto.setFare(null); //just set null if invalid
        }

        return dto;
    }

    private ITrip getTrip(long tripId) throws EntityNotFoundException {
        ITrip trip;
        try {
            trip = tripDAO.findById(tripId);
            if (trip == null)
                throw new EntityNotFoundException("Could not find trip with id " + tripId);
        } catch (PersistenceException ex) {
            throw new EntityNotFoundException("Could not find trip with id " + tripId);
        }
        return trip;
    }

    private ILocation getLocation(long locationId) throws EntityNotFoundException {
        ILocation location;
        try {
            location = locationDAO.findById(locationId);
           if (location == null)
                throw new EntityNotFoundException("Could not find location with id " + locationId);
        } catch (PersistenceException ex) {
            throw new EntityNotFoundException("Could not find location with id " + locationId);
        }
        return location;
    }
}
