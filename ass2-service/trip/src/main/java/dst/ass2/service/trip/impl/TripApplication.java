package dst.ass2.service.trip.impl;

import dst.ass2.service.trip.impl.exceptionmapper.DriverNotAvailableExceptionMapper;
import dst.ass2.service.trip.impl.exceptionmapper.EntityNotFoundExceptionMapper;
import dst.ass2.service.trip.impl.exceptionmapper.IllegalStateExceptionMapper;
import dst.ass2.service.trip.impl.exceptionmapper.InvalidTripExceptionMapper;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class TripApplication extends Application {
    private final Set<Object> singletons = new HashSet<>();
    private final Set<Class<?>> classes = new HashSet<>();

    public TripApplication() {
        singletons.add(new TripServiceResource());
        classes.add(DriverNotAvailableExceptionMapper.class);
        classes.add(EntityNotFoundExceptionMapper.class);
        classes.add(IllegalStateExceptionMapper.class);
        classes.add(InvalidTripExceptionMapper.class);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
