package dst.ass2.service.trip.impl;

import dst.ass2.service.api.trip.*;
import dst.ass2.service.api.trip.rest.ITripServiceResource;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/trips")
public class TripServiceResource implements ITripServiceResource{

    @Inject
    private ITripService tripService;

    @Override
    public Response createTrip(Long riderId, Long pickupId,  Long destinationId) throws EntityNotFoundException, InvalidTripException {
        TripDTO trip = tripService.create(riderId, pickupId, destinationId);
        if(trip == null) throw new InvalidTripException();
        return Response.ok(trip.getId()).build();
    }

    @Override
    public Response confirm(Long tripId) throws EntityNotFoundException, InvalidTripException {
        tripService.confirm(tripId);
        return Response.ok().build();
    }

    @Override
    public Response getTrip(Long tripId) throws EntityNotFoundException {
        TripDTO trip = tripService.find(tripId);
        if(trip == null) throw new EntityNotFoundException("Could not find the trip!");
        return Response.ok(trip).build();
    }

    @Override
    public Response deleteTrip(Long tripId) throws EntityNotFoundException {
        tripService.delete(tripId);
        return Response.ok().build();
    }

    @Override
    public Response addStop(Long tripId, Long locationId) throws InvalidTripException, EntityNotFoundException {
        TripDTO trip = tripService.find(tripId);
        if(!tripService.addStop(trip, locationId)) throw new InvalidTripException();
        return Response.ok(trip.getFare()).build();
    }

    @Override
    public Response removeStop(Long tripId,  Long locationId) throws InvalidTripException, EntityNotFoundException {
        TripDTO trip = tripService.find(tripId);
        if(!tripService.removeStop(trip, locationId)) throw new InvalidTripException();
        return Response.ok().build();
    }

    @Override
    public Response match(Long tripId, MatchDTO matchDTO) throws EntityNotFoundException, DriverNotAvailableException {
        tripService.match(tripId, matchDTO);
        return Response.ok().build();
    }

    @Override
    public Response complete(Long tripId, TripInfoDTO tripInfoDTO) throws EntityNotFoundException {
        tripService.complete(tripId, tripInfoDTO);
        return Response.ok().build();
    }

    @Override
    public Response cancel(Long tripId) throws EntityNotFoundException {
        tripService.cancel(tripId);
        return Response.ok().build();
    }
}
