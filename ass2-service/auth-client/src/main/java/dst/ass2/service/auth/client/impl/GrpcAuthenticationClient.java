package dst.ass2.service.auth.client.impl;

import ch.qos.logback.core.subst.Token;
import dst.ass2.service.api.auth.AuthenticationException;
import dst.ass2.service.api.auth.NoSuchUserException;
import dst.ass2.service.api.auth.proto.*;
import dst.ass2.service.auth.client.AuthenticationClientProperties;
import dst.ass2.service.auth.client.IAuthenticationClient;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import org.slf4j.LoggerFactory;

public class GrpcAuthenticationClient implements IAuthenticationClient {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(GrpcAuthenticationClient.class);

    private final ManagedChannel channel;

    private final AuthServiceGrpc.AuthServiceBlockingStub blockingStub;

    public GrpcAuthenticationClient(AuthenticationClientProperties properties) {
        LOG.debug("Opening new rpc Channel" + properties.getHost() + ":" + properties.getPort());
        channel = ManagedChannelBuilder.forAddress(properties.getHost(), properties.getPort())
                .usePlaintext()
                .build();
        blockingStub = AuthServiceGrpc.newBlockingStub(channel);
    }

    @Override
    public String authenticate(String email, String password) throws NoSuchUserException, AuthenticationException {
        AuthenticationRequest request = AuthenticationRequest.newBuilder()
                .setEmail(email)
                .setPassword(password)
                .build();
        try {
            return blockingStub.authenticate(request).getAuthToken();
        } catch (StatusRuntimeException ex) {
            LOG.error(ex.getMessage());
            if(ex.getStatus().getCode() == Status.Code.NOT_FOUND)
                throw new NoSuchUserException(ex.getMessage());
            if(ex.getStatus().getCode() == Status.Code.INTERNAL)
                throw new AuthenticationException(ex.getMessage());
            throw ex;
        }
    }

    @Override
    public boolean isTokenValid(String token) {
        TokenValidationRequest request = TokenValidationRequest.newBuilder()
                .setToken(token)
                .build();
        boolean valid;
        try {
            valid = blockingStub.validateToken(request).getValid();
        } catch (StatusRuntimeException ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
        return valid;
    }

    @Override
    public void close() {
        channel.shutdown();
    }
}
