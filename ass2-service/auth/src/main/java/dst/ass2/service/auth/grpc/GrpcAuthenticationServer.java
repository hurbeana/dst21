package dst.ass2.service.auth.grpc;

import dst.ass2.service.api.auth.AuthenticationException;
import dst.ass2.service.api.auth.IAuthenticationService;
import dst.ass2.service.api.auth.NoSuchUserException;
import dst.ass2.service.api.auth.proto.*;
import io.grpc.Status;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ApplicationScoped
public class GrpcAuthenticationServer extends AuthServiceGrpc.AuthServiceImplBase {

    @Inject
    private IAuthenticationService authenticationService;

    /**
     * Attempts to authenticate the user with the given unique email address and the given password in plain text, by
     * checking the data against the records in the database. If the credentials are successfully authenticated, the
     * service generates a new authentication token which is stored (with the users email address) in-memory and then
     * returned.
     *
     * @param request the user email and password
     */
    public void authenticate(dst.ass2.service.api.auth.proto.AuthenticationRequest request,
                             io.grpc.stub.StreamObserver<dst.ass2.service.api.auth.proto.AuthenticationResponse> responseObserver) {
        try {
            String tok;
            AuthenticationResponse.Builder builder = AuthenticationResponse.newBuilder();
            tok = authenticationService.authenticate(request.getEmail(), request.getPassword());
            builder.setAuthToken(tok);
            responseObserver.onNext(builder.build());
            responseObserver.onCompleted();
        } catch (NoSuchUserException ex) {
            responseObserver.onError(Status.NOT_FOUND
                    .withDescription(ex.getMessage())
                    .withCause(ex)
                    .asRuntimeException());
        } catch (AuthenticationException ex) {
            responseObserver.onError(Status.INTERNAL
                    .withDescription(ex.getMessage())
                    .withCause(ex)
                    .asRuntimeException());
        }
    }

    /**
     * Checks whether the given token is valid (i.e., was issued by this service and has not been invalidated).
     *
     * @param request the token to validate
     */
    public void validateToken(dst.ass2.service.api.auth.proto.TokenValidationRequest request,
                              io.grpc.stub.StreamObserver<dst.ass2.service.api.auth.proto.TokenValidationResponse> responseObserver) {
        boolean valid = false;
        TokenValidationResponse.Builder builder = TokenValidationResponse.newBuilder();
        try {
            valid = authenticationService.isValid(request.getToken());
        } catch (Exception ignored) { }
        builder.setValid(valid);
        responseObserver.onNext(builder.build());
        responseObserver.onCompleted();
    }
}
