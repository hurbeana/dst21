package dst.ass2.service.auth.grpc;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Named
@ApplicationScoped
public class GrpcServerRunner implements IGrpcServerRunner {
    @Inject
    GrpcServerProperties grpcServerProperties;

    @Inject
    GrpcAuthenticationServer grpcAuthenticationServer;

    private Server server;

    @Override
    public void run() throws IOException {
        server = ServerBuilder.forPort(grpcServerProperties.getPort())
                .addService(grpcAuthenticationServer)
                .build()
                .start();
    }

    private void stop() throws InterruptedException {
        if (server != null) {
            server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
        }
    }

    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }
}
