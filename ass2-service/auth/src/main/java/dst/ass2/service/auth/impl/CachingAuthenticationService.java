package dst.ass2.service.auth.impl;

import dst.ass1.jpa.dao.IDAOFactory;
import dst.ass1.jpa.dao.IRiderDAO;
import dst.ass1.jpa.model.IModelFactory;
import dst.ass1.jpa.model.IRider;
import dst.ass2.service.api.auth.AuthenticationException;
import dst.ass2.service.api.auth.NoSuchUserException;
import dst.ass2.service.auth.ICachingAuthenticationService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Named
@ApplicationScoped
@Transactional
public class CachingAuthenticationService implements ICachingAuthenticationService {

    @Inject
    private IModelFactory modelFactory;
    @Inject
    private IDAOFactory daoFactory;
    @PersistenceContext
    private EntityManager em;
    private IRiderDAO riderDAO;
    private HashMap<String, byte[]> pwdCache; // mail => sha1PWD
    private HashMap<String, String> tokCache; // UUIDString => mail

    private Lock readLock, writeLock;

    @PostConstruct
    public void setup() {
        riderDAO = daoFactory.createRiderDAO();
        pwdCache = new HashMap<>();
        tokCache = new HashMap<>();
        ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
        readLock = readWriteLock.readLock();
        writeLock = readWriteLock.writeLock();
        loadData();
    }

    @Override
    public void changePassword(String email, String newPassword) throws NoSuchUserException {
        IRider rider = getRider(email);
        byte[] newPwdBytes = sha1(newPassword);
        rider.setPassword(newPwdBytes);
        em.persist(rider);
        writeLock.lock();
        try {
            pwdCache.put(email, newPwdBytes);
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public String getUser(String token) {
        return tokCache.get(token);
    }

    @Override
    public boolean isValid(String token) {
        return tokCache.containsKey(token);
    }

    @Override
    public boolean invalidate(String token) {
        return tokCache.remove(token) != null;
    }

    @Override
    public String authenticate(String email, String password) throws NoSuchUserException, AuthenticationException {
        byte[] storedPwd;
        byte[] hashedPwd = this.sha1(password);
        if (hashedPwd == null)
            throw new AuthenticationException("Processing of password failed");
        readLock.lock();
        try {
            if (pwdCache.containsKey(email))
                storedPwd = pwdCache.get(email);
            else {
                IRider rider = getRider(email);
                storedPwd = rider.getPassword();
                if (storedPwd == null)
                    throw new AuthenticationException("Password retrieval failed");
                pwdCache.put(rider.getEmail(), storedPwd);
            }
            if (!Arrays.equals(hashedPwd, storedPwd))
                throw new AuthenticationException("Password is not correct");
            String tok = UUID.randomUUID().toString();
            tokCache.put(tok, email);
            return tok;
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public void loadData() {
        for (IRider rider :
                riderDAO.findAll()) {
            pwdCache.put(rider.getEmail(), rider.getPassword());
        }
    }

    @Override
    public void clearCache() {
        pwdCache.clear();
        tokCache.clear();
    }

    private IRider getRider(String email) throws NoSuchUserException {
        IRider rider;
        try {
            rider = riderDAO.findByEmail(email);
            if (rider == null) {
                throw new NoSuchUserException("Could not find rider with id " + email);
            }
        } catch (PersistenceException ex) {
            throw new NoSuchUserException("Could not find rider with id " + email);
        }
        return rider;
    }

    private byte[] sha1(String password) {
        try {
            return MessageDigest.getInstance("SHA1").digest(password.getBytes());
        } catch (NoSuchAlgorithmException ignored) { }
        return null;
    }
}
