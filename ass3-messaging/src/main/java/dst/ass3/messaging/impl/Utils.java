package dst.ass3.messaging.impl;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import dst.ass3.messaging.Constants;
import dst.ass3.messaging.Region;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class Utils {
    public final static HashMap<Region, String> regionToRoutingKey = new HashMap<>();
    public final static HashMap<String, Region> routingKeyToRegion = new HashMap<>();
    public final static HashMap<String, Region> workQueueToRegion = new HashMap<>();

    static {
        regionToRoutingKey.put(Region.AT_VIENNA, Constants.ROUTING_KEY_AT_VIENNA);
        regionToRoutingKey.put(Region.AT_LINZ, Constants.ROUTING_KEY_AT_LINZ);
        regionToRoutingKey.put(Region.DE_BERLIN, Constants.ROUTING_KEY_DE_BERLIN);

        for(Map.Entry<Region, String> entry : regionToRoutingKey.entrySet()){
            routingKeyToRegion.put(entry.getValue(), entry.getKey());
        }

        workQueueToRegion.put(Constants.QUEUE_AT_VIENNA, Region.AT_VIENNA);
        workQueueToRegion.put(Constants.QUEUE_AT_LINZ, Region.AT_LINZ);
        workQueueToRegion.put(Constants.QUEUE_DE_BERLIN, Region.DE_BERLIN);
    }

    public static Connection setupRMQConnection() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(Constants.RMQ_HOST);
        factory.setPort(Integer.parseInt(Constants.RMQ_PORT));
        factory.setUsername(Constants.RMQ_USER);
        factory.setPassword(Constants.RMQ_PASSWORD);
        try {
            return factory.newConnection();
        } catch (IOException | TimeoutException e) {
            throw new RuntimeException(e);
        }
    }

    public static Channel setupRMQChannel(Connection conn) {
        try {
            return conn.createChannel();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
