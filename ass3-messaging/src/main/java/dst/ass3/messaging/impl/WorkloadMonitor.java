package dst.ass3.messaging.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.ClientParameters;
import com.rabbitmq.http.client.domain.QueueInfo;
import dst.ass3.messaging.Constants;
import dst.ass3.messaging.IWorkloadMonitor;
import dst.ass3.messaging.Region;
import dst.ass3.messaging.WorkerResponse;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class WorkloadMonitor implements IWorkloadMonitor {
    private Client rmqClient;
    private final Connection conn;
    private final Channel channel;
    private String ownQueue;
    private final Map<Region, Queue<Double>> regionProcessTimes;

    public WorkloadMonitor(String requestsTopic) {
        regionProcessTimes = new HashMap<>();
        for (Region region :
                Region.values()) {
            regionProcessTimes.put(region, new ArrayDeque<>(11));
        }
        try {
            rmqClient = new Client(
                    new ClientParameters()
                            .url(Constants.RMQ_API_URL)
                            .username(Constants.RMQ_USER)
                            .password(Constants.RMQ_PASSWORD)
            );
        } catch (URISyntaxException | MalformedURLException e) {
            e.printStackTrace();
        }

        conn = Utils.setupRMQConnection();
        channel = Utils.setupRMQChannel(conn);
        if (ownQueue == null) {
            try {
                ownQueue = channel.queueDeclare().getQueue();
                if(requestsTopic == null) {
                    requestsTopic = "requests.*";
                }
                channel.queueBind(ownQueue, Constants.TOPIC_EXCHANGE, requestsTopic);
                channel.basicConsume(ownQueue, false, getConsumer(channel));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private DefaultConsumer getConsumer(Channel bindChannel) {
        return new DefaultConsumer(bindChannel) {
            @Override
            public void handleDelivery(String consumerTag,
                                       Envelope envelope,
                                       AMQP.BasicProperties properties,
                                       byte[] body)
                    throws IOException {

                String routingKey = envelope.getRoutingKey();
                // TODO: maybe check if contentType is not application/json
                String contentType = properties.getContentType();
                long deliveryTag = envelope.getDeliveryTag();

                WorkerResponse response = new ObjectMapper().readValue(body, WorkerResponse.class);
                if (response != null) {
                    Queue<Double> queue = regionProcessTimes.get(Utils.routingKeyToRegion.get(routingKey));
                    queue.offer(Double.valueOf(response.getProcessingTime()));
                    if (queue.size() == 11) {
                        queue.poll();
                    }
                }

                channel.basicAck(deliveryTag, false);
            }
        };
    }

    /**
     * Returns for each region the amount of waiting requests.
     *
     * @return a map
     */
    @Override
    public Map<Region, Long> getRequestCount() {
        Map<Region, Long> reqCounts = new HashMap<>();
        for (QueueInfo qInfo :
                rmqClient.getQueues()) {
            if(Utils.workQueueToRegion.containsKey(qInfo.getName()))
                reqCounts.put(Utils.workQueueToRegion.get(qInfo.getName()), qInfo.getTotalMessages());
        }
        return reqCounts;
    }

    /**
     * Returns the amount of workers for each region. This can be deduced from the amount of consumers to each
     * queue.
     *
     * @return a map
     */
    @Override
    public Map<Region, Long> getWorkerCount() {
        Map<Region, Long> reqCounts = new HashMap<>();
        for (QueueInfo qInfo :
                rmqClient.getQueues()) {
            if(Utils.workQueueToRegion.containsKey(qInfo.getName()))
                reqCounts.put(Utils.workQueueToRegion.get(qInfo.getName()), qInfo.getConsumerCount());
        }
        return reqCounts;
    }

    /**
     * Returns for each region the average processing time of the last 10 recorded requests. The data comes from
     * subscriptions to the respective topics.
     *
     * @return a map
     */
    @Override
    public Map<Region, Double> getAverageProcessingTime() {
        Map<Region, Double> reqAvgProcessingTimes = new HashMap<>();
        for (Map.Entry<Region, Queue<Double>> regionQueue :
                regionProcessTimes.entrySet()) {
            OptionalDouble avg = regionQueue.getValue().stream().mapToDouble(a -> a).average();
            reqAvgProcessingTimes.put(regionQueue.getKey(), avg.isPresent() ? avg.getAsDouble() : -1);
        }
        return reqAvgProcessingTimes;
    }

    @Override
    public void close() throws IOException {
        if(ownQueue != null) {
            channel.queueDelete(ownQueue);
        }
        if (channel != null) {  // close the channel
            try {
                channel.close();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }
        }
        if (conn != null) {  // close the connection
            conn.close();
        }
    }
}
