package dst.ass3.messaging.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;
import dst.ass3.messaging.Constants;
import dst.ass3.messaging.IRequestGateway;
import dst.ass3.messaging.TripRequest;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RequestGateway implements IRequestGateway {

    private final Connection conn;
    private Channel channel;

    public RequestGateway() {
        conn = Utils.setupRMQConnection();
        channel = Utils.setupRMQChannel(conn);

    }

    /**
     * Serializes and routes a request to the correct queue.
     *
     * @param request the request
     */
    @Override
    public void submitRequest(TripRequest request) {
        byte[] json = null;
        try {
            json = new ObjectMapper().writeValueAsBytes(request);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        try {
            // exchange
            // routingKey
            // basicProperties (header, see MessageProperties)
            // body
            channel.basicPublish(Constants.REQUEST_EXCHANGE, Utils.regionToRoutingKey.get(request.getRegion()), MessageProperties.PERSISTENT_TEXT_PLAIN, json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes any resources that may have been initialized (connections, channels, etc.)
     *
     * @throws IOException propagated exceptions
     */
    @Override
    public void close() throws IOException {
        if (channel != null) {  // close the channel
            try {
                channel.close();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }
        }
        if (conn != null) {  // close the connection
            conn.close();
        }
    }
}
