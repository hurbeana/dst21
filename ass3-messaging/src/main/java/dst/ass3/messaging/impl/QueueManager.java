package dst.ass3.messaging.impl;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import dst.ass3.messaging.Constants;
import dst.ass3.messaging.IQueueManager;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class QueueManager implements IQueueManager {

    private Connection conn;
    private Channel channel;

    /**
     * Initializes all queues or topic exchanges necessary for running the system.
     */
    @Override
    public void setUp() {
        conn = Utils.setupRMQConnection();
        channel = Utils.setupRMQChannel(conn);

        try {
            // last flag is durable
            channel.exchangeDeclare(Constants.TOPIC_EXCHANGE, "topic", true);
            channel.exchangeDeclare(Constants.REQUEST_EXCHANGE, "direct", true);

            for (int i = 0; i < Constants.WORK_QUEUES.length; i++) {
                // Name
                // Durable (the queue will survive a broker restart)
                // Exclusive (used by only one connection and the queue will be deleted when that connection closes)
                // Auto-delete (queue that has had at least one consumer is deleted when last consumer unsubscribes)
                // Arguments (optional; used by plugins and broker-specific features such as message TTL, queue length limit, etc)
                channel.queueDeclare(Constants.WORK_QUEUES[i], true, false, false, null);
                channel.queueBind(Constants.WORK_QUEUES[i], Constants.REQUEST_EXCHANGE, Constants.WORK_ROUTING_KEYS[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes all queues or topic exchanged associated with the system.
     */
    @Override
    public void tearDown() {
        try {
            for (String queue :
                    Constants.WORK_QUEUES) {
                channel.queueDelete(queue);
            }
            channel.exchangeDelete(Constants.TOPIC_EXCHANGE);
            channel.exchangeDelete(Constants.REQUEST_EXCHANGE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes underlying connection or resources, if any.
     *
     * @throws IOException propagated exceptions
     */
    @Override
    public void close() throws IOException {
        if (channel != null) {  // close the channel
            try {
                channel.close();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }
        }
        if (conn != null) {  // close the connection
            conn.close();
        }
    }
}
