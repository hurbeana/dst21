package dst.ass1.kv.impl;

import dst.ass1.kv.ISessionManager;
import dst.ass1.kv.SessionCreationFailedException;
import dst.ass1.kv.SessionNotFoundException;
import redis.clients.jedis.*;

import java.util.List;
import java.util.UUID;

public class SessionManager implements ISessionManager {

    private final JedisPool pool;

    public SessionManager(String host, Integer port) {
        this.pool = new JedisPool(new JedisPoolConfig(), host, port);
    }

    @Override
    public String createSession(Long userId, int timeToLive) throws SessionCreationFailedException {
        String uuid = UUID.randomUUID().toString();
        String user = userId.toString();
        try (Jedis jedis = pool.getResource()) {
            jedis.watch(user);
            if(jedis.exists(uuid)) {
                throw new SessionCreationFailedException();
            }
            Transaction t = jedis.multi();
            t.hset(uuid, "userId", userId.toString());
            t.set(user, uuid);
            t.expire(uuid, timeToLive);
            if(t.exec().size() == 0){
                throw new SessionCreationFailedException();
            }
        }
        return uuid;
    }

    @Override
    public void setSessionVariable(String sessionId, String key, String value) throws SessionNotFoundException {
        try (Jedis jedis = pool.getResource()) {
            if(!jedis.exists(sessionId)) {
                throw new SessionNotFoundException();
            }
            jedis.hset(sessionId, key, value);
        }
    }

    @Override
    public String getSessionVariable(String sessionId, String key) throws SessionNotFoundException {
        String value;
        try (Jedis jedis = pool.getResource()) {
            if(!jedis.exists(sessionId)) {
                throw new SessionNotFoundException();
            }
            value = jedis.hget(sessionId, key);
        }
        return value;
    }

    @Override
    public Long getUserId(String sessionId) throws SessionNotFoundException {
        long value;
        try (Jedis jedis = pool.getResource()) {
            if(!jedis.exists(sessionId)) {
                throw new SessionNotFoundException();
            }
            value = Long.parseLong(jedis.hget(sessionId, "userId"));
        }
        return value;
    }

    @Override
    public int getTimeToLive(String sessionId) throws SessionNotFoundException {
        int value;
        try (Jedis jedis = pool.getResource()) {
            value = jedis.ttl(sessionId).intValue();
            if(value < 0) throw new SessionNotFoundException();
        }
        return value;
    }

    @Override
    public String requireSession(Long userId, int timeToLive) throws SessionCreationFailedException {
        String user = userId.toString();
        try (Jedis jedis = pool.getResource()) {
            jedis.watch(user);
            if(jedis.exists(user)) {
                return jedis.get(user);
            }
            return this.createSession(userId, timeToLive);
        }
    }

    @Override
    public void close() {
        pool.close();
    }
}
